class statsCalculator {
    fun eval(a: List<Int>): String {

        val minimum = a.minOf{element -> element}
        val maximum = a.maxOf { element -> element}
        val numElements = a.count()
        val average = a.average().round(6)

        println(minimum)
        println(maximum)
        println(numElements)
        println(average)

        return """|minimum value = $minimum
                  |maximum value = $maximum
                  |number of elements in the sequence = $numElements
                  |average value = $average""".trimMargin()
    }
//holas caracola

}

fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).toDouble()
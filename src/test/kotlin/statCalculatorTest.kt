import org.junit.jupiter.api.Test

class statCalculatorTest {

    val statCal4Test = statsCalculator()

    @Test
    fun checkSimpleArray(){
        val a = listOf<Int>(6, 9, 15, -2, 92, 11)
        println(a)
        assert(statCal4Test.eval(a).equals("""|minimum value = -2
                  |maximum value = 92
                  |number of elements in the sequence = 6
                  |average value = 21.833333""".trimMargin()))
    }

    @Test
    fun checkSimpleArray1Element(){
        val a = listOf<Int>(1)
        println(a)
        assert(statCal4Test.eval(a).equals("""|minimum value = 1
                  |maximum value = 1
                  |number of elements in the sequence = 1
                  |average value = 1.0""".trimMargin()))
    }

    @Test
    fun checkSimpleArray2Elements(){
        val a = listOf<Int>(1,2)
        println(a)
        assert(statCal4Test.eval(a).equals("""|minimum value = 1
                  |maximum value = 2
                  |number of elements in the sequence = 2
                  |average value = 1.5""".trimMargin()))
    }

    @Test
    fun checkSimpleArray10Elements(){
        val a = listOf<Int>(1,1,1,1,1,1,1,1,1,1)
        println(a)
        assert(statCal4Test.eval(a).equals("""|minimum value = 1
                  |maximum value = 1
                  |number of elements in the sequence = 10
                  |average value = 1.0""".trimMargin()))
    }
}